var signer;

var contractAddress = "0x2a4c2d4dc436713533438fa4cdd157e9d8df05c0"; //put your contract address here
var ABI = [
  {
    constant: true,
    inputs: [],
    name: "getWebsite",
    outputs: [
      {
        name: "",
        type: "string"
      }
    ],
    payable: false,
    stateMutability: "view",
    type: "function"
  },
  {
    constant: false,
    inputs: [
      {
        name: "_website",
        type: "string"
      }
    ],
    name: "setWebsite",
    outputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "function"
  },
  {
    inputs: [],
    payable: false,
    stateMutability: "nonpayable",
    type: "constructor"
  }
]; // What are functions, variable names that can interact with js
var contract;

async function initialize(web3) {
  ethereum.enable();
  //0x61773200B83EBFDE3fe99120E5E30627EA3F7D89

  let provider = new ethers.providers.Web3Provider(web3.currentProvider);

  let accounts = await provider.listAccounts();
  signer = provider.getSigner(accounts[0]); //read from the blockchain is free
  //writing to the blockchain is paid

  contract = new ethers.Contract(contractAddress, ABI, signer);

  let website = await contract.getWebsite();
  e = document.getElementById("websiteLink");
  e.href = website;
}

async function setWebsite() {
  let website = document.getElementById("websiteInput").value;
  await contract.setWebsite(website);
}
