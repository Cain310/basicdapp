// pragma solidity ^0.5.10; 

// contract favWebsite{
//     string website;
//     address owner;
    
//     constructor() public {
//         owner = msg.sender;
//     }
    
//     function getWebsite() public view returns(string memory){
//      return website;   
//     }
    
//     function setWebsite(string memory _website) public {
//         require(msg.sender==owner);
//         website = _website;
        
//     }
// }


//Author - anonymous
//Solidity program to call the function.

pragma solidity ^0.5.10;

contract favWebsite{
    string website;
    address owner;
    
    constructor() public{
        owner = msg.sender;  //sends address of the owner
    }
    
    function getWebsite() public view returns(string memory){
        return website;
    }
    
    function setWebsite(string memory _website) public{
        require(msg.sender == owner);
        website = _website;
    }
}